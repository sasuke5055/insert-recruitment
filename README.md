# Agralan recruitment task
## Short description
Task was developed using Django and DRF with PostgreSQL database and docker.

## Requirements
* [Docker](https://docs.docker.com/engine/install/)
* [docker-compose](https://docs.docker.com/compose/install/)
  
If you run the application locally you will need also:
* python (>=3.8)
* pip
* other [requirements](requirements.txt)

## Project setup
### Run all in one docker
To run project go to docker-compose.yml file location and run 
```bash
$ docker-compose up --build
```

If django throws exceptions, stop docker (ctrl+c), and run just 
```bash
$ docker-compose up
```
(It sometimes happens when django is not waiting for postgres to load).

*Optional* <br> To create superuser run
```bash
$ docker exec -it <[your container id]> python manage.py createsuperuser
```
Find container id with 
```bash
$ docker container ls
```
### Local development
For easier development you can run another docker container - only with postgres db using 
```bash
$ docker-compose -f docker-compose-local.yml up
```
then f.e. create virtual env, install [requirements](requirements.txt) 
(little [help](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/)), 
apply migrations 
```bash
$ python manage.py migrate
```
and run django with 
```bash
$ python manage.py runserver
```
## Postman
Repository contains [Postman collection](Agralan.postman_collection.json).
 You can import this into postman and browse endpoints.

## Tests
Tests with coverage report can be run with 
```bash
$ bash tests.sh
```
Report will be stored in [coverage-result](coverage-result.txt) file.

## DB migration
Database was filled with random data using 
[migration](agralan/migrations/0002_fill_db.py), 
so all endpoints will return some data. Migration will be automatically applied.
