from django.contrib import admin

from agralan.models import Category, Offer

admin.site.register(Category)
admin.site.register(Offer)
