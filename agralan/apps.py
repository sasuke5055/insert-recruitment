from django.apps import AppConfig


class AgralanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'agralan'
