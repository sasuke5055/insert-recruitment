from decimal import Decimal

from django.db import migrations
from random import randint, choice

CATEGORY_NAMES = ['cheese', 'meat', 'bread', 'juice', 'sweets', 'spices']
# Random 200 worlds from www.mit.edu/~ecprice/wordlist.10000 longer than 5 chars
OFFER_NAMES = ['investigated', 'subsidiaries', 'infrared', 'galaxy', 'instructors', 'article', 'penetration', 'darwin',
               'opposed', 'hungarian', 'illegal', 'gourmet', 'titten', 'points', 'entered', 'delayed', 'towers',
               'laboratories', 'optics', 'boulder', 'connection', 'season', 'available', 'carries', 'expertise',
               'elections', 'companies', 'miniature', 'presently', 'olympus', 'memorabilia', 'anyway', 'observed',
               'catherine', 'classics', 'visited', 'generated', 'sunglasses', 'others', 'precisely', 'biology',
               'cleaning', 'occasionally', 'connecting', 'cowboy', 'option', 'visits', 'riders', 'playboy', 'dentists',
               'motors', 'fairly', 'perception', 'introduce', 'beauty', 'phillips', 'inline', 'spyware', 'invisible',
               'transferred', 'belong', 'coffee', 'portuguese', 'attitudes', 'suddenly', 'contractors', 'merchant',
               'reliance', 'supervision', 'aggregate', 'advertisers', 'compressed', 'cancellation', 'explain',
               'complicated', 'calvin', 'vagina', 'condos', 'flooring', 'products', 'approaches', 'rocket',
               'geographic', 'dispute', 'cotton', 'producers', 'liberal', 'fields', 'herein', 'deficit', 'functional',
               'computation', 'finger', 'imaging', 'storage', 'define', 'convinced', 'brought', 'freelance',
               'polyphonic']
OFFER_DESC_WORLD = ['thousand', 'italic', 'breach', 'worldwide', 'morocco', 'instant', 'differently', 'fifteen',
                    'palace', 'documentcreatetextnode', 'troops', 'terminal', 'accepted', 'fabric', 'jessica', 'serial',
                    'invitation', 'advisory', 'conclusions', 'medicines', 'turned', 'kansas', 'princess', 'treating',
                    'vibrator', 'viewers', 'archives', 'respectively', 'slowly', 'nominations', 'survey', 'admission',
                    'orlando', 'regulated', 'christmas', 'materials', 'matched', 'discharge', 'quantities', 'awarded',
                    'sexuality', 'companion', 'vector', 'height', 'maldives', 'symposium', 'briefly', 'attributes',
                    'marathon', 'fragrance', 'battlefield', 'screensavers', 'paperback', 'hotelscom', 'transexuales',
                    'hitachi', 'rabbit', 'soldier', 'guided', 'column', 'biology', 'supports', 'facility', 'regions',
                    'respondents', 'accompanying', 'gloves', 'syntax', 'publish', 'mastercard', 'temporarily',
                    'olympus', 'moldova', 'certification', 'complaint', 'police', 'surprising', 'shopper', 'transexual',
                    'investing', 'worlds', 'province', 'airline', 'minneapolis', 'mission', 'elections', 'damages',
                    'jennifer', 'tariff', 'detection', 'optical', 'kinase', 'reservations', 'pleasant', 'ecuador',
                    'definitions', 'delivery', 'announced', 'himself', 'recognized']


def add_categories(apps, _):
    Category = apps.get_model("agralan", "Category")
    for n in Category.objects.all():
        super(Category, n).delete()
    for i, c_name in enumerate(CATEGORY_NAMES):
        new_category = Category(
            id=i,
            name=c_name,
            ordering=randint(1, 100))
        new_category.save()


def add_offers(apps, _):
    Category = apps.get_model("agralan", "Category")
    Offer = apps.get_model("agralan", "Offer")

    categories = list(Category.objects.all())

    for i, off_name in enumerate(OFFER_NAMES):
        new_offer = Offer(
            id=i,
            title=off_name,
            description=" ".join(choice(OFFER_DESC_WORLD) for _ in range(randint(3, 7))),
            price=Decimal(randint(10, 100000) / 100),
            category=choice(categories),
        )
        new_offer.save()
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('agralan', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_categories),
        migrations.RunPython(add_offers)
    ]
