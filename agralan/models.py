from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=150, verbose_name='Name')
    ordering = models.IntegerField(verbose_name='Ordering', default=0)

    def __str__(self):
        return f'{self.id} - {self.name}'

    class Meta:
        verbose_name_plural = 'Categories'


class Offer(models.Model):
    title = models.CharField(max_length=150, verbose_name='Title')
    description = models.TextField(verbose_name='Description')
    price = models.DecimalField(max_digits=19, decimal_places=2, verbose_name='Price')
    category = models.ForeignKey(Category, verbose_name='Category', on_delete=models.CASCADE)
    created_at = models.DateTimeField(verbose_name='Creation date', auto_now_add=True)

    def __str__(self):
        return f'{self.id} - {self.title} - {self.price}'

    def get_cost(self):
        return str(self.price)

    class Meta:
        verbose_name_plural = 'Offers'
