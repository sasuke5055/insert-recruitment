from decimal import Decimal

from rest_framework import serializers

from agralan.models import Category, Offer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        extra_kwargs = {
            'ordering': {'read_only': True}
        }


class OfferSerializer(serializers.ModelSerializer):
    category = serializers.IntegerField(source='category.id')
    price = serializers.CharField(source='get_cost')

    class Meta:
        model = Offer
        fields = ('id', 'title', 'price', 'category')


class OfferDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = '__all__'

    def validate_price(self, value: Decimal):
        """
        Check if :param value is grater than 0
        """
        if value <= Decimal(0):
            raise serializers.ValidationError("Price should be greater than 0")
        return value