import random
from unittest import TestCase

from django.test import TransactionTestCase, SimpleTestCase
from rest_framework import status
from rest_framework.test import APIClient

from agralan.models import Category, Offer
from agralan.serializers import CategorySerializer, OfferSerializer, OfferDetailsSerializer


class CategoryTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.categories = Category.objects.all()

    def test_get_list(self):
        resp = self.client.get('/agralan/category')

        # check category count
        self.assertEqual(len(self.categories), len(resp.data), 'Categories count doesn\'t match')

        # check category ordering
        self.assertEqual(
            sorted(resp.data, key=lambda it: it['ordering']), resp.data, 'Categories aren\'t properly sorted'
        )

        # check all categories
        for c, r in zip(sorted(self.categories, key=lambda it: (it.ordering, it.id)), resp.data):
            self.assertEqual(CategorySerializer(c).data, dict(r), 'Categories doesn\'t match')

    def test_retrieve(self):
        resp = self.client.get('/agralan/category/0')

        # check if retrieve is disabled
        self.assertEqual(resp.data["message"], 'Retrieve function is disabled.', 'Retrieve is not disabled')
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN, 'Status code should be 403')

    def test_patch(self):
        resp = self.client.patch('/agralan/category/0')

        # check if patch is disabled
        self.assertEqual(resp.data["message"], 'Patch function is disabled.', 'Patch is not disabled')
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN, 'Status code should be 403')

    def test_post_put_delete(self):
        payload = {
            "name": "Test category"
        }
        payload_modified = {
            "name": "Test category modified"
        }

        ## Test POST
        post_resp = self.client.post('/agralan/category', data=payload)

        # check status code
        self.assertEqual(post_resp.status_code, status.HTTP_201_CREATED, 'Category creations status code is wrong')

        self.added_category_id = post_resp.data['id']
        self.added_category = Category.objects.get(id=self.added_category_id)

        # check if category was added properly
        self.assertEqual(
            CategorySerializer(self.added_category).data, post_resp.data, 'Category wasn\'t added properly'
        )

        ## Test PUT
        put_resp = self.client.put(f'/agralan/category/{self.added_category_id}', data=payload_modified)

        # check status code
        self.assertEqual(put_resp.status_code, status.HTTP_200_OK, 'Category update status code is wrong')

        self.updated_category = Category.objects.get(id=self.added_category_id)

        # check if category was updated properly
        self.assertEqual(
            CategorySerializer(self.updated_category).data, put_resp.data, 'Category wasn\'t updated properly'
        )

        ## Test DELETE
        resp = self.client.delete(f'/agralan/category/{self.added_category_id}')

        # check respond
        self.assertEqual(
            resp.data["message"],
            f'Successfully deleted object with id \'{self.added_category_id}\'',
            f'Cannot delete category with id {self.added_category_id}'
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK, 'Category delete status code is wrong')

    def test_all_fields_required(self):
        empty_post_resp = self.client.post('/agralan/category')
        self.assertEqual(empty_post_resp.status_code, status.HTTP_400_BAD_REQUEST, 'Category should require all fields')
        for d in empty_post_resp.data:
            self.assertEqual(
                str(empty_post_resp.data[d][0]),
                'This field is required.',
                'Category should require field \'{d}\''
            )


class OffersGetTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.offers = Offer.objects.all()
        temp = Category.objects.order_by('?')
        self.random_category = temp.first()
        self.second_random_category = temp[1:2].first()

    def test_get_list(self):
        resp = self.client.get('/agralan/offers')

        # check offer count
        self.assertEqual(len(self.offers), len(resp.data), 'Offers count doesn\'t match')

        # check all offers
        for o, r in zip(self.offers, resp.data):
            self.assertEqual(OfferSerializer(o).data, r, 'Offers doesn\'t match')

    def test_get_for_category(self):
        resp = self.client.get(f'/agralan/offers?category={self.random_category.id}')

        self.assertEqual(
            resp.data,
            OfferSerializer(self.offers.filter(category__id=self.random_category.id), many=True).data,
            'Offers filtering failed'
        )

    def test_single_offer(self):
        # Might fail if there is no offers
        random_offer = random.choice(self.offers)
        resp = self.client.get(f'/agralan/offers/{random_offer.id}')

        self.assertEqual(resp.data, OfferDetailsSerializer(random_offer).data, 'Single offer doesn\'t match')

    def test_post_put_delete(self):
        payload = {
            "title": "Test offer",
            "price": 20.05,
            "category": self.random_category.pk,
            "description": "Test description"
        }
        payload_modified = {
            "title": "Test offer updated",
            "price": 22.36,
            "category": self.second_random_category.pk,
            "description": "Test description updated"
        }

        ## Test POST
        post_resp = self.client.post('/agralan/offers', data=payload)

        # check status code
        self.assertEqual(post_resp.status_code, status.HTTP_201_CREATED, 'Offer creations status code is wrong')

        self.added_offer_id = post_resp.data['id']
        self.added_offer = Offer.objects.get(id=self.added_offer_id)

        # check if offer was added properly
        self.assertEqual(
            OfferDetailsSerializer(self.added_offer).data, post_resp.data, 'Offer wasn\'t added properly'
        )

        ## Test PUT
        put_resp = self.client.put(f'/agralan/offers/{self.added_offer_id}', data=payload_modified)

        # check status code
        self.assertEqual(put_resp.status_code, status.HTTP_200_OK, 'Offer update status code is wrong')

        self.updated_offer = Offer.objects.get(id=self.added_offer_id)

        # check if offer was updated properly
        self.assertEqual(
            OfferDetailsSerializer(self.updated_offer).data, put_resp.data, 'Offer wasn\'t updated properly'
        )

        ## Test DELETE
        resp = self.client.delete(f'/agralan/offers/{self.added_offer_id}')

        # check respond
        self.assertEqual(
            resp.data["message"],
            f'Successfully deleted object with id \'{self.added_offer_id}\'',
            f'Cannot delete offer with id {self.added_offer_id}'
        )
        self.assertEqual(resp.status_code, status.HTTP_200_OK, 'Offer delete status code is wrong')

    def test_all_fields_required(self):
        empty_post_resp = self.client.post('/agralan/offers')
        self.assertEqual(empty_post_resp.status_code, status.HTTP_400_BAD_REQUEST, 'Offer should require all fields')
        for d in empty_post_resp.data:
            self.assertEqual(
                str(empty_post_resp.data[d][0]),
                'This field is required.',
                'Offer should require field \'{d}\''
            )


class DataValidationTests(TestCase):

    def test_price_validation(self):
        data = {
            "title": "Test offer",
            "price": -20.05,
            "category": 0,
            "description": "Test description"
        }
        serializer = OfferDetailsSerializer(data=data)
        self.assertFalse(serializer.is_valid())
        self.assertEqual(str(serializer.errors['price'][0]), 'Price should be greater than 0')
