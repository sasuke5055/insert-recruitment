from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter

from agralan.views import CategoryViewSet, OfferViewSet

router = SimpleRouter(trailing_slash=False)
router.register(r'category', CategoryViewSet, 'category')
router.register(r'offers', OfferViewSet, 'offers')

urlpatterns = [
    path('', include(router.urls))
]
