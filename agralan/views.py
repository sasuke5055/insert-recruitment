from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.request import Request
from rest_framework.response import Response

from agralan.models import Category, Offer
from agralan.serializers import CategorySerializer, OfferDetailsSerializer, OfferSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.order_by('ordering', 'id')

    def retrieve(self, request, pk=None, **kwargs):
        response = {'message': 'Retrieve function is disabled.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def partial_update(self, request, pk=None, **kwargs):
        response = {'message': 'Patch function is disabled.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None, **kwargs):
        super().destroy(request, pk=pk, **kwargs)
        return Response({'message': f'Successfully deleted object with id \'{pk}\''}, status=status.HTTP_200_OK)


class OfferViewSet(viewsets.ModelViewSet):
    serializer_dict = {
        'default': OfferDetailsSerializer,
        'list': OfferSerializer,
    }
    queryset = Offer.objects.all()

    def get_serializer_class(self):
        serializer_class = self.serializer_dict.get(self.action, None)
        return serializer_class if serializer_class else self.serializer_dict['default']

    def list(self, request: Request, *args, **kwargs):
        category_id = request.query_params.get('category', None)
        self.queryset = Offer.objects.filter(category_id=category_id) if category_id else self.get_queryset()
        return super().list(request, *args, **kwargs)

    def partial_update(self, request, pk=None, **kwargs):
        response = {'message': 'Patch function is disabled.'}
        return Response(response, status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None, **kwargs):
        super().destroy(request, pk=pk, **kwargs)
        return Response({'message': f'Successfully deleted object with id \'{pk}\''}, status=status.HTTP_200_OK)
